#!/usr/bin/env python
# coding: utf-8

import rospy
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image, CompressedImage
from geometry_msgs.msg import Point
import cv2
import numpy as np
import os
import sys
from darkflow.net.build import TFNet

# CONSTANTS
# ---------------------------
# ROS
ROS_HOSTNAME = "waffle"
TOPIC_SUB = ROS_HOSTNAME + "/raspicam_node/image/compressed"
# TOPIC_SUB = ROS_HOSTNAME + "/usb_cam/image_raw/compressed"
TOPIC_PUB = "coords_ball"

# Configuration of Darkflow
PATH_TO_CFG = '/home/linux/catkin_ws/src/follower/src/darkflow/cfg/tiny-yolo_custom.cfg'
PATH_TO_LOAD = '/home/linux/catkin_ws/src/follower/src/darkflow/bin/tiny-yolo.weights'

# Thresholding
SCORE_MIN = 0.7

options = {"model": PATH_TO_CFG, "load": PATH_TO_LOAD, "threshold": SCORE_MIN}

def rescale_frame(frame):
    width = 300
    height = 300
    dim = (width, height)
    return cv2.resize(frame, dim, interpolation =cv2.INTER_AREA)


class Ball_Detection:
    """
    Class used to detect the ball
    The program detects a soccer ball using API Tensorflow Object Detection and OpenCV
    Coordinates of the balls are published to topic "coords_ball"
    """

    def __init__(self):

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber(TOPIC_SUB, CompressedImage, self.callback)
        # self.image_sub = rospy.Subscriber(TOPIC_SUB, Image, self.callback)
        self.publisher_coords = rospy.Publisher(TOPIC_PUB, Point, queue_size=5)

        # Initial point
        self.point = Point()
        self.point.x = 0
        self.point.y = 0
        self.point.z = 0

        self.tfnet = TFNet(options)
        self.tfnet.load_from_ckpt()


    def callback(self, image):
        """
        Used when an image is received

        Args:
            image (CompressedImage): the received image.

        Returns:
            None
        """

        try:
            has_found = False
            cv_image = self.bridge.compressed_imgmsg_to_cv2(image, desired_encoding="passthrough")

            # cv_image = rescale_frame(cv_image)
            rows, cols, _ = cv_image.shape
            predictions = self.tfnet.return_predict(cv_image)
            for result in predictions:
                if result['confidence'] > SCORE_MIN:
                    x_top = result['topleft']['x']
                    y_top = result['topleft']['y']
                    x_bot = result['bottomright']['x']
                    y_bot = result['bottomright']['y']
                    center = (int((x_bot + x_top)/2), int((y_bot + y_top)/2))
                    coords_top_left = (int(x_top), int(y_top))
                    coords_bot_right = (int(x_bot), int(y_bot))
                    text = result['label'] + ", " + str(round(result['confidence']*100, 2)) + " %"

                    cv2.rectangle(cv_image, coords_top_left,
                                      coords_bot_right, (125, 255, 51), thickness=1)
                    cv2.putText(cv_image, text, coords_top_left,
                                    cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
                    cv2.circle(cv_image, center, 1, (255, 255, 0), 2)                  

            cv2.imshow("Image", cv_image)

            # Shutdown
            if cv2.waitKey(3) == ord('q'):
                print("Shutdown")
                rospy.signal_shutdown("shutdown")

        except CvBridgeError as e:
            print(e)


    def publish_coords(self):
        self.publisher_coords.publish(self.point)

if __name__ == "__main__":
    bd = Ball_Detection()
    # Init node 'ball_detection
    rospy.init_node("ball_detection", anonymous=True)
    # Loop
    rospy.spin()

