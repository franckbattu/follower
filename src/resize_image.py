import cv2 

def resize(image, width=300, height=300):
    """
    Resize an image

    Args:
        image (numpy.ndarray): the image to resize
        width (int): the width of the desired size
        height (int): the height of the desired size

    Returns:
        the image resized 
    """
    return cv2.resize(image, (width, height), interpolation =cv2.INTER_AREA)

