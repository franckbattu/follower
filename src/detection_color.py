#!/usr/bin/env python
# coding: utf-8

import rospy
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image, CompressedImage
from geometry_msgs.msg import Point
import cv2
import numpy as np

# CONSTANTS
# ---------------------------
# ROS
TOPIC_SUB = "waffle/raspicam_node/image/compressed"
TOPIC_PUB = "coords_ball"

# DETECTION
RADIUS_MIN = 10
COLOR_MIN = np.array([5, 150, 170])
COLOR_MAX = np.array([15, 255, 255])


class Ball_Detection:
    """
    Class used to detect the ball
    """

    def __init__(self):
        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber(TOPIC_SUB, CompressedImage, self.callback)
        self.publisher_coords = rospy.Publisher(TOPIC_PUB, Point, queue_size=5)

    def callback(self, image):
        """
        Used when an image is received

        Args:
            image (CompressedImage): the received image.
        """

        try:
            cv_image = self.bridge.compressed_imgmsg_to_cv2(
                image, "bgr8")
            height, width, _ = cv_image.shape
            self.detectionBall(cv_image)

        except CvBridgeError as e:
            print(e)

    def detectionBall(self, image):
        """
        Detection of the ball

        Args:
            image (numpy.ndarray): the image to use to detect the ball.

        Returns:
            None.
        """
        image_blurred = cv2.GaussianBlur(image, (11, 11), 0)
        image_hsv = cv2.cvtColor(image_blurred, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(image_hsv, COLOR_MIN, COLOR_MAX)

        # Noise supression
        mask = cv2.erode(mask, None, iterations=5)
        mask = cv2.dilate(mask, None, iterations=7)

        res = cv2.bitwise_and(image, image, mask=mask)

        _, contours, _ = cv2.findContours(
            mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        if contours:

            circle = max(contours, key=cv2.contourArea)
            (centerX, centerY), radius = cv2.minEnclosingCircle(circle)

            centerX = int(centerX)
            centerY = int(centerY)
            radius = int(radius)

            if radius > RADIUS_MIN:
                cv2.circle(image, (centerX, centerY),
                           radius, (255, 255, 0), 2)
                point = Point()
                point.x = centerX
                point.y = centerY
                self.publisher_coords.publish(point)

        cv2.imshow("Image hsv", image_hsv)
        cv2.imshow("Mask", mask)
        cv2.imshow("Res", res)
        cv2.imshow("Image", image)
        cv2.waitKey(3)


if __name__ == "__main__":
    bd = Ball_Detection()
    rospy.init_node("ball_detection", anonymous=True)
    rospy.spin()
