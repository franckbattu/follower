#!/usr/bin/env python
# coding: utf-8

import rospy
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image, CompressedImage
from geometry_msgs.msg import Point
import cv2
import numpy as np
import tensorflow as tf
import os
import sys
import object_detection
from object_detection.utils import label_map_util
from PIL import Image

# CONSTANTS
# ---------------------------
# ROS
ROS_HOSTNAME = "waffle"
TOPIC_SUB = ROS_HOSTNAME + "/raspicam_node/image/compressed"
# TOPIC_SUB = ROS_HOSTNAME + "/usb_cam/image_raw/compressed"
TOPIC_PUB = "coords_ball"

# Configuration of Tensorflow
PATH_TO_GRAPH = '/home/linux/catkin_ws/src/follower/data/models/frozen_inference_graph.pb'
PATH_TO_LABELS = '/home/linux/catkin_ws/src/follower/data/labels/label_map.pbtxt'
NUMBER_CLASSES = 1

# Thresholding
SCORE_MIN = 0.4

# Color
RADIUS_MIN = 10
COLOR_MIN = np.array([5, 150, 170])
COLOR_MAX = np.array([15, 255, 255])

from sklearn.cluster import KMeans
from collections import Counter

def rescale_frame(frame):
    width = 512
    height = 512
    dim = (width, height)
    return cv2.resize(frame, dim, interpolation = cv2.INTER_AREA)


class Ball_Detection:
    """
    Class used to detect the ball
    The program detects a soccer ball using API Tensorflow Object Detection and OpenCV
    Coordinates of the balls are published to topic "coords_ball"
    """

    def __init__(self):

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber(TOPIC_SUB, CompressedImage, self.callback)
        self.publisher_coords = rospy.Publisher(TOPIC_PUB, Point, queue_size=1)

        # Initial point
        self.point = Point()
        self.point.x = 0
        self.point.y = 0
        self.point.z = 0

        self.sess = tf.Session()
        self.sess.graph.as_default()

        # Read graph
        with tf.gfile.FastGFile(PATH_TO_GRAPH, 'rb') as f:
            graph_def = tf.GraphDef()
            file_read = f.read()
            graph_def.ParseFromString(file_read)
        tf.import_graph_def(graph_def, name='')

        # Open labels file and get categories
        self.label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        self.categories = label_map_util.convert_label_map_to_categories(
            self.label_map, NUMBER_CLASSES)

        # Dictionnary with labels
        self.category_dict = label_map_util.create_category_index(
            self.categories)

        # Get all the tensors
        self.detection_scores = self.sess.graph.get_tensor_by_name(
            'detection_scores:0')
        self.number_detections = self.sess.graph.get_tensor_by_name(
            'num_detections:0')
        self.detection_classes = self.sess.graph.get_tensor_by_name(
            'detection_classes:0')
        self.detection_boxes = self.sess.graph.get_tensor_by_name(
            'detection_boxes:0')

    def callback(self, image):
        """
        Used when an image is received

        Args:
            image (CompressedImage): the received image.

        Returns:
            None
        """

        try:
            has_found = False
            cv_image = self.bridge.compressed_imgmsg_to_cv2(image, desired_encoding="passthrough")
            rows, cols, _ = cv_image.shape
            detections, scores, boxes, classes = self.classify(cv_image) 

            for i in range(detections):
                score = scores[i]

                if score > SCORE_MIN:

                    # Class detected
                    classId = int(classes[i])
                    name = self.category_dict[classId]['name']

                    if name == "soccer_ball":

                        # Coords of the rectangle
                        # boxes[i] = [top, left, bottom, right]
                        location = boxes[i]
                        x_top = location[1] * cols
                        y_top = location[0] * rows
                        x_bot = location[3] * cols
                        y_bot = location[2] * rows

                        center = (int((x_bot + x_top)/2), int((y_bot + y_top)/2))
                        coords_top_left = (int(x_top), int(y_top))
                        coords_bot_right = (int(x_bot), int(y_bot))

                        ball = cv_image[int(y_top):int(y_bot), int(x_top):int(x_bot)]
                        ball = rescale_frame(ball)
                        image_hsv = cv2.cvtColor(ball, cv2.COLOR_BGR2HSV)
                        mask = cv2.inRange(image_hsv, COLOR_MIN, COLOR_MAX)

                        res = cv2.bitwise_and(ball, ball, mask=mask)

                        _, contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 
                        
                        for contour in contours:

                            approx = cv2.approxPolyDP(contour, 0.01*cv2.arcLength(contour, True), True)

                            if not 3 <= len(approx) <= 5:
                                has_found = True
                                self.point.x = center[0]
                                self.point.y = center[1]
                                self.point.z = 0
                                print("Ball")
                            else:
                                has_found = False   
                                print("No ball")

                        text = name + ", " + str(round(score*100, 2)) + " %"

                        # Display on image
                        cv2.rectangle(cv_image, coords_top_left,
                                    coords_bot_right, (125, 255, 51), thickness=1)
                        cv2.putText(cv_image, text, coords_top_left,
                                    cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
                        cv2.circle(cv_image, center, 1, (255, 255, 0), 2)


            if not has_found:
                self.point.x = 0
                self.point.y = 0
                self.point.z = -1
            
            self.publish_coords()
            cv2.imshow("Image", cv_image)

            # Shutdown
            if cv2.waitKey(3) == ord('q'):
                print("Shutdown")
                rospy.signal_shutdown("shutdown")

        except CvBridgeError as e:
            print(e)

    def classify(self, image):
        res = self.sess.run([self.number_detections, self.detection_scores, self.detection_boxes, self.detection_classes],
                                feed_dict={'image_tensor:0': np.expand_dims(image, axis=0)})

        # Collect informations (tensors -> arrays)
        detections = int(res[0][0])
        scores = res[1][0]
        boxes = res[2][0]
        classes = res[3][0]

        return detections, scores, boxes, classes

    def publish_coords(self):
        self.publisher_coords.publish(self.point)

if __name__ == "__main__":
    bd = Ball_Detection()
    # Init node 'ball_detection
    rospy.init_node("ball_detection", anonymous=True)
    # Loop
    rospy.spin()

