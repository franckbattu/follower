#!/usr/bin/env python
# coding: utf-8

import rospy
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image, CompressedImage
from geometry_msgs.msg import Point
import cv2
import numpy as np
import tensorflow as tf
from object_detection.utils import label_map_util
import resize_image

# ROS
# ---------------------------
# Hostname of the ROS robot : waffle or burger2
ROS_HOSTNAME = "waffle"

# Subscriber topic to get the images from camera
TOPIC_SUB = ROS_HOSTNAME + "/raspicam_node/image/compressed"

# Publisher topic to send the ball coordinates
TOPIC_PUB = "coords_ball"

# CONFIGURATION OF TENSORFLOW
# ---------------------------
# Path to the frozen inference graph used
PATH_TO_GRAPH = '/home/linux/catkin_ws/src/follower/data/models/frozen_inference_graph.pb'

# Path to the label map
PATH_TO_LABELS = '/home/linux/catkin_ws/src/follower/data/labels/label_map.pbtxt'

# Number of classes in the label map file
NUMBER_CLASSES = 1

# Thresholding (between 0 and 1)
SCORE_MIN = 0.1


class Ball_Detection:
    """
    Class used to detect the ball
    The program detects a soccer ball using API Tensorflow Object Detection and OpenCV
    Coordinates of the balls are published to topic "coords_ball"
    """

    def __init__(self):

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber(TOPIC_SUB, CompressedImage, self.callback)
        self.publisher_coords = rospy.Publisher(TOPIC_PUB, Point, queue_size=5)

        # Initial point
        self.point = Point()
        self.point.x = 0
        self.point.y = 0
        self.point.z = 0

        self.sess = tf.Session()
        self.sess.graph.as_default()

        # Read frozen inference graph
        with tf.gfile.FastGFile(PATH_TO_GRAPH, 'rb') as f:
            graph_def = tf.GraphDef()
            file_read = f.read()
            graph_def.ParseFromString(file_read)
        tf.import_graph_def(graph_def, name='')

        # Open labels file and get categories
        self.label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        self.categories = label_map_util.convert_label_map_to_categories(
            self.label_map, NUMBER_CLASSES)

        # Dictionnary with labels
        self.category_dict = label_map_util.create_category_index(
            self.categories)

        # Get all the placeholder tensors
        self.detection_scores = self.sess.graph.get_tensor_by_name('detection_scores:0')
        self.number_detections = self.sess.graph.get_tensor_by_name('num_detections:0')
        self.detection_classes = self.sess.graph.get_tensor_by_name('detection_classes:0')
        self.detection_boxes = self.sess.graph.get_tensor_by_name('detection_boxes:0')


    def callback(self, image):
        """
        Executed when an image is received

        Args:
            image (CompressedImage): the received image.

        Returns:
            None
        """

        try:
            # By default, the ball is not found
            ball_found = False

            cv_image = self.bridge.compressed_imgmsg_to_cv2(image, desired_encoding="passthrough")
            cv_image = resize_image.resize(cv_image)

            rows, cols, _ = cv_image.shape

            # Run the inference graph
            detections, scores, boxes, classes = self.classify(cv_image) 

            for i in range(detections):
                score = scores[i]

                if score > SCORE_MIN:

                    # Get the class of the detected object
                    classId = int(classes[i])
                    name = self.category_dict[classId]['name']

                    if name == "soccer_ball":
                        ball_found = True

                        # Coords of the rectangle
                        # boxes[i] = [top, left, bottom, right]
                        location = boxes[i]
                        x_top = location[1] * cols
                        y_top = location[0] * rows
                        x_bot = location[3] * cols
                        y_bot = location[2] * rows
                        center = (int((x_bot + x_top)/2), int((y_bot + y_top)/2))
                        coords_top_left = (int(x_top), int(y_top))
                        coords_bot_right = (int(x_bot), int(y_bot))

                        text = name + ", " + str(round(score*100, 2)) + " %"

                        # Display on image
                        cv2.rectangle(cv_image, coords_top_left,
                                      coords_bot_right, (125, 255, 51), thickness=1)
                        cv2.putText(cv_image, text, coords_top_left,
                                    cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
                        cv2.circle(cv_image, center, 1, (255, 255, 0), 2)


            # If the ball is not found, the robot will turn on itself
            if not ball_found:
                self.point.x = 0
                self.point.y = 0
                self.point.z = -1
            
            self.publish_coords()

            # Display on the image
            cv2.imshow("Image", cv_image)

            # Shutdown
            if cv2.waitKey(3) == ord('q'):
                print("Shutdown")
                rospy.signal_shutdown("shutdown")

        except CvBridgeError as e:
            print(e)

    def classify(self, image):
        """
        Runs the inference graph to detect objects

        Args:
            image(numpy.ndarray): the image used for the detection

        Returns:
            the numbers of detections, the scores / boxes / classes of every detection
        """
        res = self.sess.run([self.number_detections, self.detection_scores, self.detection_boxes, self.detection_classes],
                                feed_dict={'image_tensor:0': np.expand_dims(image, axis=0)})

        # Collect informations (tensors -> arrays)
        detections = int(res[0][0])
        scores = res[1][0]
        boxes = res[2][0]
        classes = res[3][0]

        return detections, scores, boxes, classes


    def publish_coords(self):
        """
        Publish the ball coordinates

        Returns:
            None
        """
        self.publisher_coords.publish(self.point)

# Main
if __name__ == "__main__":
    bd = Ball_Detection()
    rospy.init_node("ball_detection", anonymous=True)
    rospy.spin()

