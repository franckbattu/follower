#!/usr/bin/env python
# encoding: utf-8

import rospy
from geometry_msgs.msg import Point
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

# ROS
# ---------------------------
# Hostname of the ROS robot : waffle or burger2
ROS_HOSTNAME = "waffle"

# Subscriber topic to get the ball coordinates
TOPIC_SUB_COORDS = "coords_ball"

# Subscriber topic to get the data from LDS
TOPIC_SUB_LASER = ROS_HOSTNAME + "/scan"

# Publisher topic to send the velocity commands
TOPIC_PUB = ROS_HOSTNAME + "/cmd_vel"

# IMAGE
# ---------------------------
WIDTH = 640

# ROBOTS
# ---------------------------
# BURGER
# MAX_ANGLE = 2.84  # rad/s
# MAX_SPEED = 0.22  # m/s

# WAFFLE
MAX_ANGLE = 1.82  # rad/s
MAX_SPEED = 0.26  # m/s

# DISTANCES
# ---------------------------
# Minimum stop distance of the robot
DISTANCE_MIN_STOP = 0.4 # m

# Ball centered interval
DISTANCE_MIN_NORM = 0.05


class Move:
    """
    Class used to move the robot according to the ball coordinates

    The program receives the ball coordinates from a ROS topic, and sends the velocity commands
    """

    def __init__(self):
        rospy.init_node("move", anonymous=True)
        rospy.on_shutdown(self.shutdown)
        self.rate = rospy.Rate(10)
        
        # ROS
        self.coords_ball = rospy.Subscriber(TOPIC_SUB_COORDS, Point, self.callbackBall)
        self.laser = rospy.Subscriber(TOPIC_SUB_LASER, LaserScan, self.callbackScan)
        self.vel_pub = rospy.Publisher(TOPIC_PUB, Twist, queue_size=1)

        # Velocity commands
        self.speed = Twist()

        # Initial moves
        self.speed.angular.z = MAX_ANGLE * 0.1
        self.speed.linear.x = 0

        self.distance = 0
        self.range = 1
        self.distance_norm = self.distance / self.range
        self.middle = WIDTH / 2


    def callbackBall(self, coords):
        """
        Executed when ball coordinates are received

        Args:
            coords(geometry_msgs.msg.Point): the ball coordinates
        
        Returns:
            None
        """

        # The robot is too close to an obstacle 
        if (0 < self.distance < DISTANCE_MIN_STOP):
            self.stop()

        else:

            # No ball found
            if coords.z == -1:
                self.speed.linear.x = 0
                self.speed.angular.z = MAX_ANGLE * 0.1
            
            # Ball found
            else:
                delta = self.middle - coords.x
                delta_norm = delta/(WIDTH/2)
                self.erreurs += delta_norm
                
                # The ball is not centered
                if (abs(delta_norm) > DISTANCE_MIN_NORM):
                    self.speed.linear.x = MAX_SPEED
                    self.speed.angular.z = MAX_ANGLE * delta_norm

                # The ball is centered
                else:
                    self.speed.linear.x = MAX_SPEED
                    self.speed.angular.z = 0


    def getDistance(self, data):
        """
        This function is used to get the distance between the robot and an obstacle

        Args:
            data(Array): the ranges array
        
        Returns:
            the distance between the robot and an obstacle in front of it
        """
        angles = [359, 0, 1]
        res = []
        for angle in angles:
            if res > 0:
                res.append(data[angle])
        return min(res)

    def callbackScan(self, data):
        """
        Executed when LDS data is received

        Args:
            data(sensor_msgs.msg.LaserScan): the LDS data
        
        Returns:
            None
        """
        self.range = data.range_max
        self.distance = self.getDistance(data.ranges)
        self.distance_norm = self.distance / self.range


    def send_velocity_cmd(self):
        """
        Send the velocity commands
        """
        self.vel_pub.publish(self.speed)


    def stop(self):
        """
        Send stop velocity commands
        """
        self.speed.linear.x = 0
        self.speed.angular.z = 0


    def shutdown(self):
        """
        Shutdown ROS
        """
        self.stop()
        self.vel_pub.publish(self.speed)
        rospy.sleep(1)

# Main
if __name__ == "__main__":
    try:
        move = Move()
        while not rospy.is_shutdown():
            move.send_velocity_cmd()
            move.rate.sleep()
    except:
        move.loginfo("Terminated")
