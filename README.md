# Package ROS follower

Suivi d'un ballon par un robot ROS   

## Lancement

Pour lancer le package, il suffit d'exécuter la commande :
```
roslaunch follower follow.launch
```

Cette commande lance les deux fichiers suivants :
* **detection_tensorflow.py**
* **move.py**

Pour que le package fonctionne, il faut également que les images venant du robot soient publiées.

## Contenu du dossier **src**
Deux fichiers sont principalement utilisées :
* **detection_tensorflow.py** : script permettant la détection du ballon avec l'API Tensorflow
* **move.py** : script permettant le déplacement du robot

D'autres fichiers existent également dans le package. Ce sont principalement des tests de détections, qui peuvent être supprimés ou améliorés :
* **detection_tensorflow_shape.py** : script permettant la détection du ballon avec l'API Tensorflow et la vérification d'une forme ronde
* **detection_tensorflow_color.py** : script permettant la detection du ballon avec l'API Tensorflow et la vérification de la couleur
* **detection_darkflow.py** : script permettant la détection du ballon avec l'API Darkflow (Darknet + Tensorflow)

Fichiers *utils* :
* **resize_image.py** : script permettant de redimensionner les images   

## Contenu du dossier **data**   
* **labels** : contient les fichiers *label_map.pbtxt*
* **models** : contients les models .pb (*frozen_inference_graph*)